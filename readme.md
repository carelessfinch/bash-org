# File Organizer Script

This Bash script helps organize files from a source directory to a destination directory based on different criteria.

## Usage

```bash
./organize_files.sh <source_directory> <destination_directory> [options]
```

## Options
* `-s ext|date`: Organize by extension or creation date.
* `-d`: Delete original files after organizing.
* `-e extensions`: Exclude specific file extensions (comma-separated).
* `-l logfile`: Specify the log file for actions.

## Examples

```bash
./org.sh -s ext source_folder destination_folder
```

```bash
./org.sh -d -s ext -e png -l log_file source_folder destination_folder
```
