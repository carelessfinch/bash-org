#!/bin/bash

usage() {
    echo "Usage: $0 <source_directory> <destination_directory> [options]"
    echo "Options:"
    echo "  -s ext|date   Organize by extension or creation date."
    echo "  -d            Delete original files after organizing."
    echo "  -e extensions Exclude specific file extensions (comma-separated)."
    echo "  -l logfile    Specify the log file for actions."
}

while getopts "s:de:l:" opt; do
    case $opt in
        s) SORT_METHOD=$OPTARG ;;
        d) DELETE_ORIGINAL=1 ;;
        e) EXCLUDE_EXTENSIONS=$OPTARG ;;
        l) LOGFILE=$OPTARG ;;
        *) usage; exit 1 ;;
    esac
done

shift $((OPTIND-1))

if [ $# -lt 2 ]; then
    usage
    exit 1
fi

SRC_DIR=$1
DEST_DIR=$2

[ -z "$SORT_METHOD" ] && SORT_METHOD="ext"


copy_and_rename_files() {
    local src_file=$1
    local dest_file=$2

    local counter=1
    while [ -f "$dest_file" ]; do
        local subdir=$(basename -- "$(dirname -- "$dest_file")")
        local filename=$(basename -- "$src_file")
        local extension="${filename##*.}"
        local filename_no_ext="${filename%.*}"
        dest_file="${DEST_DIR}/${subdir}/${filename_no_ext}_${counter}.${extension}"
        ((counter++))
    done

    cp "$src_file" "$dest_file"

    [ -n "$LOGFILE" ] && echo "Moved: $src_file -> $dest_file" >> "$LOGFILE"
}

generate_summary() {
    echo "Summary Report:"
    echo "Number of folders created: $(find "$DEST_DIR" -mindepth 1 -type d | wc -l)"
    echo "Number of files transferred: $(find "$DEST_DIR" -type f | wc -l)"
    local dir
    for dir in "$DEST_DIR"/*; do
        [ -d "$dir" ] && echo "Files in $(basename "$dir"): $(find "$dir" -type f | wc -l)"
    done
}


should_exclude() {
    local file_ext=$1
    IFS=',' read -ra EXCLUDED <<< "$EXCLUDE_EXTENSIONS"
    for ext in "${EXCLUDED[@]}"; do
        [ "$ext" == "$file_ext" ] && return 0
    done
    return 1
}

delete_original_files() {
    find "$SRC_DIR" -type f | while read file; do
        local filename=$(basename -- "$file")
        local extension="${filename##*.}"
        if [ -n "$EXCLUDE_EXTENSIONS" ] && should_exclude "$extension"; then
            continue
        fi
        rm "$file"
        [ -n "$LOGFILE" ] && echo "Deleted: $file" >> "$LOGFILE"
    done
    echo "Original files deleted."
}



main() {
    find "$SRC_DIR" -type f | while read file; do
        local filename=$(basename -- "$file")
        local extension="${filename##*.}"

        if [ -n "$EXCLUDE_EXTENSIONS" ] && should_exclude "$extension"; then
            continue
        fi

        local dest_subdir=""
        if [ "$SORT_METHOD" == "ext" ]; then
            dest_subdir="$extension"
        elif [ "$SORT_METHOD" == "date" ]; then
            # Extract creation date and format it
            local creation_date=$(stat -c %y "$file" | cut -d ' ' -f1)
            dest_subdir=$(date -d "$creation_date" +%Y-%m-%d)
        fi

        local dest_path="${DEST_DIR}/${dest_subdir}"
        [ ! -d "$dest_path" ] && mkdir -p "$dest_path"

        copy_and_rename_files "$file" "${dest_path}/${filename}"
    done

    generate_summary
    if [ -n "$DELETE_ORIGINAL" ]; then
        delete_original_files
    fi
}

main
